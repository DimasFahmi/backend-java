package com.backend.java.service;

import java.util.List;

import com.backend.java.entity.KaryawanIndex;
import com.backend.java.model.TambahDataKaryawan;

public interface AllService {

	List<KaryawanIndex> findAll();

	int insertData(TambahDataKaryawan emp);

	List<KaryawanIndex> updateData(TambahDataKaryawan emp);

	int deleteData(TambahDataKaryawan emp);

	int updateDataExecute(TambahDataKaryawan emp);
}
