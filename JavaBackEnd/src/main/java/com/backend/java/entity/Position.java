package com.backend.java.entity;

public class Position {

	private Long id;
	private String code;
	private String name;
	private int isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public Position(Long id, String code, String name, int isDelete) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.isDelete = isDelete;
	}

}
